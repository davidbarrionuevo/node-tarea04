const fs = require("fs");

const crearTabla = (numero) => {
  const nroTabla = numero;
  const saltoLlinea = "\n";
  let tabla = `Tabla del ${nroTabla}${saltoLlinea}`;
  for (let i = 1; i <= 10; i++) {
    let lineaTabla = `${nroTabla} x ${i} = ${i * nroTabla}${saltoLlinea}`;
    tabla = tabla + lineaTabla;
  }
  return tabla;
};

const crearArchivo = (n) => {
  const numero = n;

  const nombrePrefijo = `tabla_del_${numero}`;
  const nombreExtension = ".txt";
  const fecha = new Date();
  const hoy = fecha.getDate();
  const mls = fecha.getTime();
  const saltoLlinea = "\n";
  let nombreArchivo = `${nombrePrefijo}_${hoy}-${mls}_${nombreExtension}`;
  fs.writeFile(nombreArchivo, crearTabla(numero), { encoding: "utf8" }, (e,data) => {
    if (e) console.log("Error:" + e);
    else {
      console.log("Archivo Grabado Exitosamente." + saltoLlinea);
      console.log(`El archivo ${nombreArchivo} contiene:`);
      setTimeout(() => {
        console.log(fs.readFileSync(nombreArchivo, "utf8"));
      }, 3000);
    }
  });
};


module.exports = {
    crearArchivo
}
